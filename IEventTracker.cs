﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTMTestApp1
{
    public interface IEventTracker
    {
        void Track(string eventId);
        void Track(string eventId, string paramName, string value);
        void Track(string eventId, IDictionary<string, string> parameters);
    }
}
