﻿using System.Collections.Generic;
using Android.OS;
using Firebase.Analytics;

namespace GTMTestApp1
{
    public class FirebaseCart
    {
        private Bundle itemParams = new Bundle();
        private List<FirebaseProduct> cartItems = new List<FirebaseProduct>();
        public void SetCurrency(string currency)
        {
            itemParams.PutString(FirebaseAnalytics.Param.Currency, currency);
        }
        public void SetValue(double value)
        {
            itemParams.PutDouble(FirebaseAnalytics.Param.Value, value);
        }
        public void SetCouponCode(string couponCode)
        {
            itemParams.PutString(FirebaseAnalytics.Param.Coupon, couponCode);
        }
        public void AddProduct(FirebaseProduct product)
        {
            cartItems.Add(product);
        }
        public List<FirebaseProduct> GetCartItems()
        {
            return this.cartItems;
        }
        public Bundle GetParams()
        {
            List<IParcelable> productList = new List<IParcelable>();
            for(int i = 0; i < this.cartItems.Count; i++)
            {
                productList.Add(cartItems[i].GetParams());
            }
            itemParams.PutParcelableArrayList(FirebaseAnalytics.Param.Items, productList);
            return itemParams;
        }
    }
}