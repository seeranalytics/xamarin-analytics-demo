﻿using System.Collections.Generic;
using Android.OS;
using Firebase.Analytics;

namespace GTMTestApp1
{
    public class FirebaseTransaction
    {
        private Bundle itemParams = new Bundle();
        private List<IParcelable> cartItems = new List<IParcelable>();
        public void SetId(string id)
        {
            itemParams.PutString(FirebaseAnalytics.Param.TransactionId, id);
        }
        public void SetAffiliation(string affiliation)
        {
            itemParams.PutString(FirebaseAnalytics.Param.Affiliation, affiliation);
        }
        public void SetCurrency(string currency)
        {
            itemParams.PutString(FirebaseAnalytics.Param.Currency, currency);
        }
        public void SetValue(double value)
        {
            itemParams.PutDouble(FirebaseAnalytics.Param.Value, value);
        }
        public void SetTaxAmount(double taxAmount)
        {
            itemParams.PutDouble(FirebaseAnalytics.Param.Tax, taxAmount);
        }
        public void SetShippingAmount(double shippingAmount)
        {
            itemParams.PutDouble(FirebaseAnalytics.Param.Shipping, shippingAmount);
        }
        public void SetCouponCode(string couponCode)
        {
            itemParams.PutString(FirebaseAnalytics.Param.Coupon, couponCode);
        }
        public void AddProduct(FirebaseProduct product)
        {
            cartItems.Add(product.GetParams());
        }
        public Bundle GetParams()
        {
            itemParams.PutParcelableArrayList(FirebaseAnalytics.Param.Items, cartItems);
            return itemParams;
        }
    }
}