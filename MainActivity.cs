﻿using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Views;
using AndroidX.AppCompat.App;
using AndroidX.AppCompat.Widget;
using Firebase.Analytics;
using Google.Android.Material.FloatingActionButton;
using Google.Android.Material.Snackbar;
using System;
using System.Collections.Generic;

namespace GTMTestApp1
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            FloatingActionButton fab = FindViewById<FloatingActionButton>(Resource.Id.fab);
            fab.Click += FabOnClick;

            Android.Widget.Button button1 = FindViewById<Android.Widget.Button>(Resource.Id.viewitem);
            button1.Click += ViewItemOnClick;

            Android.Widget.Button button2 = FindViewById<Android.Widget.Button>(Resource.Id.addtocart);
            button2.Click += AddToCartOnClick;

            Android.Widget.Button button3 = FindViewById<Android.Widget.Button>(Resource.Id.removefromcart);
            button3.Click += RemoveFromCartOnClick;

            Android.Widget.Button button4 = FindViewById<Android.Widget.Button>(Resource.Id.viewcart);
            button4.Click += ViewCartOnClick;

            Android.Widget.Button button5 = FindViewById<Android.Widget.Button>(Resource.Id.begincheckout);
            button5.Click += BeginCheckoutOnClick;

            Android.Widget.Button button6 = FindViewById<Android.Widget.Button>(Resource.Id.shipping);
            button6.Click += ShippingInfoOnClick;

            Android.Widget.Button button7 = FindViewById<Android.Widget.Button>(Resource.Id.payment);
            button7.Click += PaymentInfoOnClick;

            Android.Widget.Button button8 = FindViewById<Android.Widget.Button>(Resource.Id.purchase);
            button7.Click += PurchaseOnClick;
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void ViewItemOnClick(object sender, EventArgs eventArgs)
        {
            var tracker = new EventTrackerAndroid();

            var product1 = new FirebaseProduct();
            product1.SetId("3029");
            product1.SetName("Essential Baking Company Pumpkin Pie – 9 in.");
            product1.SetCategory("Featured & Seasonal");
            product1.SetVariant("Pumpkin"); // optional
            product1.SetBrand("Essential Baking Company");
            product1.SetPrice(16.99);
            product1.SetQuantity(2);

            tracker.TrackProductView(product1);
        }

        private void AddToCartOnClick(object sender, EventArgs eventArgs)
        {
            var tracker = new EventTrackerAndroid();

            var product1 = new FirebaseProduct();
            product1.SetId("3029");
            product1.SetName("Essential Baking Company Pumpkin Pie – 9 in.");
            product1.SetCategory("Featured & Seasonal");
            product1.SetVariant("Pumpkin"); // optional
            product1.SetBrand("Essential Baking Company");
            product1.SetPrice(16.99);
            product1.SetQuantity(2);

            tracker.TrackAddToCart(product1);
        }

        private void RemoveFromCartOnClick(object sender, EventArgs eventArgs)
        {
            var tracker = new EventTrackerAndroid();

            var product1 = new FirebaseProduct();
            product1.SetId("3029");
            product1.SetName("Essential Baking Company Pumpkin Pie – 9 in.");
            product1.SetCategory("Featured & Seasonal");
            product1.SetVariant("Pumpkin"); // optional
            product1.SetBrand("Essential Baking Company");
            product1.SetPrice(16.99);
            product1.SetQuantity(2);

            tracker.TrackRemoveFromCart(product1);
        }
        private void ViewCartOnClick(object sender, EventArgs eventArgs)
        {
            var tracker = new EventTrackerAndroid();

            var product1 = new FirebaseProduct();
            product1.SetId("3029");
            product1.SetName("Essential Baking Company Pumpkin Pie – 9 in.");
            product1.SetCategory("Featured & Seasonal");
            product1.SetVariant("Pumpkin"); // optional
            product1.SetBrand("Essential Baking Company");
            product1.SetPrice(16.99);
            product1.SetQuantity(2);

            var cart = new FirebaseCart();
            cart.SetCurrency("USD");
            cart.SetValue(16.99 * 2);
            cart.AddProduct(product1);

            tracker.TrackCartView(cart);
        }
        private void BeginCheckoutOnClick(object sender, EventArgs eventArgs)
        {
            var tracker = new EventTrackerAndroid();

            var product1 = new FirebaseProduct();
            product1.SetId("3029");
            product1.SetName("Essential Baking Company Pumpkin Pie – 9 in.");
            product1.SetCategory("Featured & Seasonal");
            product1.SetVariant("Pumpkin"); // optional
            product1.SetBrand("Essential Baking Company");
            product1.SetPrice(16.99);
            product1.SetQuantity(2);

            var cart = new FirebaseCart();
            cart.SetCurrency("USD");
            cart.SetValue(16.99 * 2);
            cart.AddProduct(product1);

            tracker.TrackBeginCheckout(cart);
        }
        private void ShippingInfoOnClick(object sender, EventArgs eventArgs)
        {
            var tracker = new EventTrackerAndroid();

            var product1 = new FirebaseProduct();
            product1.SetId("3029");
            product1.SetName("Essential Baking Company Pumpkin Pie – 9 in.");
            product1.SetCategory("Featured & Seasonal");
            product1.SetVariant("Pumpkin"); // optional
            product1.SetBrand("Essential Baking Company");
            product1.SetPrice(16.99);
            product1.SetQuantity(2);

            var cart = new FirebaseCart();
            cart.SetCurrency("USD");
            cart.SetValue(16.99 * 2);
            cart.AddProduct(product1);

            tracker.TrackShippingInfo(cart, "14d");
        }
        private void PaymentInfoOnClick(object sender, EventArgs eventArgs)
        {
            var tracker = new EventTrackerAndroid();

            var product1 = new FirebaseProduct();
            product1.SetId("3029");
            product1.SetName("Essential Baking Company Pumpkin Pie – 9 in.");
            product1.SetCategory("Featured & Seasonal");
            product1.SetVariant("Pumpkin"); // optional
            product1.SetBrand("Essential Baking Company");
            product1.SetPrice(16.99);
            product1.SetQuantity(2);

            var cart = new FirebaseCart();
            cart.SetCurrency("USD");
            cart.SetValue(16.99 * 2);
            cart.AddProduct(product1);

            tracker.TrackPaymentInfo(cart, "VISA");
        }
        private void PurchaseOnClick(object sender, EventArgs eventArgs)
        {
            var tracker = new EventTrackerAndroid();

            var product1 = new FirebaseProduct();
            product1.SetId("3029");
            product1.SetName("Essential Baking Company Pumpkin Pie – 9 in.");
            product1.SetCategory("Featured & Seasonal");
            product1.SetVariant("Pumpkin"); // optional
            product1.SetBrand("Essential Baking Company");
            product1.SetPrice(16.99);
            product1.SetQuantity(2);

            var transaction = new FirebaseTransaction();
            transaction.SetCurrency("USD");
            transaction.SetId("SBF0000000042");
            transaction.SetTaxAmount(2.22);
            transaction.SetShippingAmount(4.44);
            transaction.SetValue(16.99 * 2 + 2.22 + 4.44);
            transaction.AddProduct(product1);

            tracker.TrackPurchase(transaction);
        }
        private void FabOnClick(object sender, EventArgs eventArgs)
        {


            FirebaseAnalytics eventTracker = FirebaseAnalytics.GetInstance(Application.Context);

            //Bundle product1 = new Bundle();
            //product1.PutString(FirebaseAnalytics.Param.ItemId, "3029");
            //product1.PutString(FirebaseAnalytics.Param.ItemName, "Essential Baking Company Pumpkin Pie – 9 in.");
            //product1.PutString(FirebaseAnalytics.Param.ItemCategory, "Featured & Seasonal");
            //product1.PutString(FirebaseAnalytics.Param.ItemVariant, "Pumpkin"); // optionsl
            //product1.PutString(FirebaseAnalytics.Param.ItemBrand, "Essential Baking Company");
            //product1.PutDouble(FirebaseAnalytics.Param.Price, 16.99);
            //product1.PutInt(FirebaseAnalytics.Param.Quantity, 2);

            //Bundle product2 = new Bundle();
            //product2.PutString(FirebaseAnalytics.Param.ItemId, "2997");
            //product2.PutString(FirebaseAnalytics.Param.ItemName, "Seasonal Harvest Produce Box");
            //product2.PutString(FirebaseAnalytics.Param.ItemCategory, "Produce");
            //product2.PutString(FirebaseAnalytics.Param.ItemBrand, "Pacific Coast Fruit Company");
            //product2.PutDouble(FirebaseAnalytics.Param.Price, 34.95);
            //product2.PutInt(FirebaseAnalytics.Param.Quantity, 1);

            //var cartProducts = new List<IParcelable>();
            //cartProducts.Add(product1);
            //cartProducts.Add(product2);

            //Bundle cart = new Bundle();
            //cart.PutString(FirebaseAnalytics.Param.Currency, "USD");
            //cart.PutDouble(FirebaseAnalytics.Param.Value, 51.94);
            //cart.PutParcelableArrayList(FirebaseAnalytics.Param.Items, cartProducts);

            //FirebaseAnalytics eventTracker = FirebaseAnalytics.GetInstance(Application.Context);
            //eventTracker.LogEvent(FirebaseAnalytics.Event.ViewCart, cart);




            //            Bundle product1 = new Bundle();
            //product1.PutString(FirebaseAnalytics.Param.ItemId, "3029");
            //product1.PutString(FirebaseAnalytics.Param.ItemName, "Essential Baking Company Pumpkin Pie – 9 in.");
            //product1.PutString(FirebaseAnalytics.Param.ItemCategory, "Featured & Seasonal");
            //product1.PutString(FirebaseAnalytics.Param.ItemVariant, "Pumpkin"); // optionsl
            //product1.PutString(FirebaseAnalytics.Param.ItemBrand, "Essential Baking Company");
            //product1.PutDouble(FirebaseAnalytics.Param.Price, 16.99);
            //product1.PutInt(FirebaseAnalytics.Param.Quantity, 1);

            //FirebaseAnalytics eventTracker = FirebaseAnalytics.GetInstance(Application.Context);
            //eventTracker.LogEvent(FirebaseAnalytics.Event.ViewItem, cart);

            /*
            adb shell setprop debug.firebase.analytics.app .none.

             adb shell setprop debug.firebase.analytics.app com.seerinteractive.gtmtestapp1
             adb shell setprop log.tag.FA VERBOSE
             adb shell setprop log.tag.FA-SVC VERBOSE
             adb logcat -v time -s FA FA-SVC
            */


            var tracker = new EventTrackerAndroid();

            //var firebaseAnalytics = FirebaseAnalytics.GetInstance(Application.Context);
            //Bundle b = new Bundle();
            //b.PutString(FirebaseAnalytics.Param.ItemId, "42");
            //b.PutString(FirebaseAnalytics.Param.ItemName, "The Meaning of Life");
            //b.PutString(FirebaseAnalytics.Param.ContentType, "book");
            //firebaseAnalytics.LogEvent(FirebaseAnalytics.Event.SelectContent, b);


            var product1 = new FirebaseProduct();
            product1.SetId("3029");
            product1.SetName("Essential Baking Company Pumpkin Pie – 9 in.");
            product1.SetCategory("Featured & Seasonal");
            product1.SetVariant("Pumpkin"); // optional
            product1.SetBrand("Essential Baking Company");
            product1.SetPrice(16.99);
            product1.SetQuantity(2);

            var product2 = new FirebaseProduct();
            product2.SetId("2997");
            product2.SetName("Seasonal Harvest Produce Box");
            product2.SetCategory("Produce");
            product2.SetBrand("Pacific Coast Fruit Company");
            product2.SetPrice(34.95);
            product2.SetQuantity(1);

            //var transaction = new FirebaseTransaction();
            //transaction.SetId("SBF0000001");
            //transaction.SetTaxAmount(2.34);
            //transaction.SetShippingAmount(8.88);
            //transaction.SetValue(80.15);
            //transaction.SetCouponCode("FALLDRIVE");
            //transaction.SetCurrency("CAD");
            //transaction.AddProduct(product1);
            //transaction.AddProduct(product2);



            //Bundle transaction = new Bundle();
            //transaction.PutString(FirebaseAnalytics.Param.TransactionId, "SBF0000001");
            //transaction.PutString(FirebaseAnalytics.Param.Currency, "USD");
            //transaction.PutString(FirebaseAnalytics.Param.Coupon, "FALLDRIVE");
            //transaction.PutDouble(FirebaseAnalytics.Param.Tax, 2.34);
            //transaction.PutDouble(FirebaseAnalytics.Param.Shipping, 8.88);
            //transaction.PutDouble(FirebaseAnalytics.Param.Value, 80.15);
            //transaction.PutParcelableArrayList(FirebaseAnalytics.Param.Items, cartProducts);

            //eventTracker.LogEvent(FirebaseAnalytics.Event.Purchase, transaction);


            //Bundle eventParams = new Bundle();
            //eventParams.PutString(FirebaseAnalytics.Param.Currency, "USD");
            //eventParams.PutString(FirebaseAnalytics.Param.Coupon, "FALLDRIVE");
            //eventParams.PutDouble(FirebaseAnalytics.Param.Value, 80.15);
            //eventParams.PutParcelableArrayList(FirebaseAnalytics.Param.Items, cartProducts);

            //eventTracker.LogEvent(FirebaseAnalytics.Event.BeginCheckout, eventParams);


            //Bundle eventParams = new Bundle();
            //eventParams.PutString(FirebaseAnalytics.Param.Currency, "USD");
            //eventParams.PutString(FirebaseAnalytics.Param.Coupon, "FALLDRIVE");
            //eventParams.PutDouble(FirebaseAnalytics.Param.Value, 80.15);
            //eventParams.PutString(FirebaseAnalytics.Param.ShippingTier, "7d"); // frequency: 7d or 14d
            //eventParams.PutParcelableArrayList(FirebaseAnalytics.Param.Items, cartProducts);

            //eventTracker.LogEvent(FirebaseAnalytics.Event.AddShippingInfo, eventParams);

            //Bundle eventParams = new Bundle();
            //eventParams.PutString(FirebaseAnalytics.Param.Currency, "USD");
            //eventParams.PutString(FirebaseAnalytics.Param.Coupon, "FALLDRIVE");
            //eventParams.PutDouble(FirebaseAnalytics.Param.Value, 80.15);
            //eventParams.PutString(FirebaseAnalytics.Param.PaymentType, "PAYMENT-TERMS");
            //eventParams.PutParcelableArrayList(FirebaseAnalytics.Param.Items, cartProducts);

            //eventTracker.LogEvent(FirebaseAnalytics.Event.AddPaymentInfo, eventParams);


            //tracker.TrackPurchase(transaction);

            //tracker.TrackProductView(product1);
            //tracker.TrackAddToCart(product1);
            //tracker.TrackAddToCart(product2);
            //tracker.TrackRemoveFromCart(product2);
            //tracker.TrackPurchase(transaction);

            FirebaseCart cart = new FirebaseCart();
            cart.SetCurrency("USD");
            cart.SetValue(50.18);
            cart.SetCouponCode("FALLSALE");
            cart.AddProduct(product1);
            cart.AddProduct(product2);
            tracker.TrackCartView(cart);


            Android.Views.View view = (Android.Views.View)sender;
            Snackbar.Make(view, "Replace with your own action > ", Snackbar.LengthLong)
                .SetAction("Action", (Android.Views.View.IOnClickListener)null).Show();

        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
