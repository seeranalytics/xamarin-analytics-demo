﻿using System.Collections.Generic;
using Android.OS;
using Firebase.Analytics;

namespace GTMTestApp1
{
    public class EventTrackerAndroid : IEventTracker
    {
        private string GetCurrency()
        {
            return "USD";
        }
        private double GetLineItemValue(FirebaseProduct p)
        {
            Bundle productParams = p.GetParams();
            double price = productParams.GetDouble(FirebaseAnalytics.Param.Price);
            int quantity = productParams.GetInt(FirebaseAnalytics.Param.Quantity);
            double value = price * quantity;
            return value;
        }
        public void Track(string eventId)
        {
            Track(eventId, new Bundle());
        }

        public void Track(string eventId, string paramName, string paramValue)
        {
            Track(eventId, new Dictionary<string, string>
            {
                {paramName, paramValue}
            });
        }

        public void Track(string eventName, IDictionary<string, string> parameters)
        {
            if (parameters == null)
            {
                Track(eventName, new Bundle());
                return;
            }

            var bundle = new Bundle();
            foreach (var param in parameters)
            {
                bundle.PutString(param.Key, param.Value);
            }

            Track(eventName, bundle);
        }

        public void Track(string eventName, Bundle eventParams)
        {
            var firebaseAnalytics = FirebaseAnalytics.GetInstance(Android.App.Application.Context);
            firebaseAnalytics.LogEvent(eventName, eventParams);
        }

        public void TrackScreen(string screenName)
        {
            Track(screenName);
        }

        public void TrackProductView(FirebaseProduct product)
        {
            var productParams = product.GetParams();

            var products = new List<IParcelable>();
            products.Add(productParams);

            double cartValue = productParams.GetDouble(FirebaseAnalytics.Param.Price);

            Bundle eventParams = new Bundle();
            eventParams.PutString(FirebaseAnalytics.Param.Currency, GetCurrency());
            eventParams.PutDouble(FirebaseAnalytics.Param.Value, cartValue);
            eventParams.PutParcelableArrayList(FirebaseAnalytics.Param.Items, products);

            Track(FirebaseAnalytics.Event.ViewItem, eventParams);
        }

        public void TrackAddToCart(FirebaseProduct product)
        {
            var productParams = product.GetParams();

            var products = new List<IParcelable>();
            products.Add(productParams);

            double lineItemValue = GetLineItemValue(product);

            Bundle eventParams = new Bundle();
            eventParams.PutString(FirebaseAnalytics.Param.Currency, GetCurrency());
            eventParams.PutDouble(FirebaseAnalytics.Param.Value, lineItemValue);
            eventParams.PutParcelableArrayList(FirebaseAnalytics.Param.Items, products);

            Track(FirebaseAnalytics.Event.AddToCart, eventParams);
        }

        public void TrackCartView(FirebaseCart cart)
        {
            Track(FirebaseAnalytics.Event.ViewCart, cart.GetParams());
        }

        public void TrackRemoveFromCart(FirebaseProduct product)
        {
            var productParams = product.GetParams();

            var products = new List<IParcelable>();
            products.Add(productParams);

            double lineItemValue = GetLineItemValue(product);

            Bundle eventParams = new Bundle();
            eventParams.PutString(FirebaseAnalytics.Param.Currency, GetCurrency());
            eventParams.PutDouble(FirebaseAnalytics.Param.Value, lineItemValue);
            eventParams.PutParcelableArrayList(FirebaseAnalytics.Param.Items, products);

            Track(FirebaseAnalytics.Event.RemoveFromCart, eventParams);
        }

        public void TrackBeginCheckout(FirebaseCart cart)
        {
            var cartParams = cart.GetParams();
            Track(FirebaseAnalytics.Event.BeginCheckout, cartParams);
        }

        public void TrackShippingInfo(FirebaseCart cart, string shippingType)
        {
            var cartParams = cart.GetParams();
            cartParams.PutString(FirebaseAnalytics.Param.ShippingTier, shippingType);
            Track(FirebaseAnalytics.Event.AddShippingInfo, cartParams);
        }

        public void TrackPaymentInfo(FirebaseCart cart, string paymentType)
        {
            var cartParams = cart.GetParams();
            cartParams.PutString(FirebaseAnalytics.Param.PaymentType, paymentType);
            Track(FirebaseAnalytics.Event.AddPaymentInfo, cartParams);
        }

        public void TrackPurchase(FirebaseTransaction transaction)
        {
            var transactionParams = transaction.GetParams();
            Track(FirebaseAnalytics.Event.Purchase, transactionParams);
        }
    }

}