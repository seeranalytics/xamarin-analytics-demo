﻿using Android.OS;
using Firebase.Analytics;

namespace GTMTestApp1
{
    public class FirebaseProduct
    {
        private Bundle itemParams = new Bundle();
        public void SetId(string id)
        {
            itemParams.PutString(FirebaseAnalytics.Param.ItemId, id);
        }
        public void SetName(string name)
        {
            itemParams.PutString(FirebaseAnalytics.Param.ItemName, name);
        }
        public void SetCategory(string category)
        {
            itemParams.PutString(FirebaseAnalytics.Param.ItemCategory, category);
        }
        public void SetVariant(string variant)
        {
            itemParams.PutString(FirebaseAnalytics.Param.ItemVariant, variant);
        }
        public void SetBrand(string brand)
        {
            itemParams.PutString(FirebaseAnalytics.Param.ItemBrand, brand);
        }
        public void SetPrice(double price)
        {
            itemParams.PutDouble(FirebaseAnalytics.Param.Price, price);
        }
        public void SetQuantity(int quantity)
        {
            itemParams.PutInt(FirebaseAnalytics.Param.Quantity, quantity);
        }
        public Bundle GetParams()
        {
            return itemParams;
        }
    }
}